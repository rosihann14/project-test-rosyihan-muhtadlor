import React from "react";
// JSX
import HeroSlider, { Slide, Nav, OverlayContainer } from "hero-slider";

// Images
import bg from '../assets/img/bg.jpg'
import about from '../assets/img/about-bg.jpg'

const ImgMove = () => {
  return (
    <HeroSlider
      slidingAnimation="left_to_right"
      orientation="horizontal"
      initialSlide={1}
      onBeforeChange={(previousSlide, nextSlide) =>
        console.log("onBeforeChange", previousSlide, nextSlide)
      }
      onChange={nextSlide => console.log("onChange", nextSlide)}
      onAfterChange={nextSlide => console.log("onAfterChange", nextSlide)}
      style={{
        backgroundColor: "rgba(0, 0, 0, 0.33)"
      }}
      // settings={{
      //   // slidingDuration: 250,
      //   // slidingDelay: 100,
      //   // shouldAutoplay: true,
      //   // shouldDisplayButtons: true,
      //   // autoplayDuration: 5000,
      //   // height: '100vh'
      // }}
    >
      <Slide
        background={{
          backgroundImageSrc: about,
          backgroundAttachment: "fixed"
        }}
      />

      <Slide
        background={{
          backgroundImageSrc: bg,
          backgroundAttachment: "fixed"
        }}
      />
      <Nav />
    </HeroSlider>
  );
};

export default ImgMove;
