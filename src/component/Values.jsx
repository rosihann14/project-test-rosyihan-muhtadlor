import main from "../assets/css/main.module.css";
import balanceScale from "../assets/img/balance-scale.png";
import bank from "../assets/img/bank.png";
import lightbulb from "../assets/img/balance-scale.png";

import React from "react";

function Values() {
  return (
    <div
      style={{
        marginTop: "40px",
      }}
    >
      {/* <div>
            <h3>Our Values</h3>
        </div> */}
      <h3 style={{ textAlign: "center" }}>OUR VALUES</h3>
      <div className={main.ourValues}>
        <div className={main.contentValues1}>
          <img src={lightbulb} width={32} alt="" />
          <h4 style={{ marginBottom: "10px", marginTop:"10px" }}>INNOVATIVE</h4>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime
            mollitia, molestiae quas vel sint commodi{" "}
          </p>
        </div>
        <div className={main.contentValues2}>
          <img src={bank} width={32} alt="" />
          <h4 style={{ marginBottom: "10px", marginTop:"10px" }}>LOYALTY</h4>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime
            mollitia, molestiae quas vel sint commodi{" "}
          </p>
        </div>

        <div className={main.contentValues3}>
          <img src={balanceScale} width={32} alt="" />
          <h4 style={{ marginBottom: "10px", marginTop:"10px" }}>RESPECT</h4>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime
            mollitia, molestiae quas vel sint commodi{" "}
          </p>
        </div>
      </div>
    </div>
  );
}

export default Values;
