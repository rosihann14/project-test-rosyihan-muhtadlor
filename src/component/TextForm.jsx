import main from "../assets/css/main.module.css";
import React, { useState } from "react";

function TextForm() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [error, setError] = useState(false);
  const [errorEmail, setErrorEmail] = useState(false);

  // const emailValidation = ()=> {
  //   const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  // }

  const handleSubmit = (e) => {
    e.preventDefault();
    const regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    if (name.length == 0 || message.length == 0) {
      setError(true);
    }
    if (!regex.test(email) || email.length == 0) {
      // console.log('benar')
      setErrorEmail(true);
    }
    else if(regex.test(email)){
      setErrorEmail(false);
    }
    if (name && email && regex.test(email)) {
      console.log(name, email, message);
    }
  };

  return (
    <div
      style={{
        marginTop: "40px",
      }}
    >
      <h3 style={{ textAlign: "center" }}>CONTACT US</h3>
      <div className={main.form}>
        <form onSubmit={handleSubmit}>
          <label>
            <p>Nama:</p>
            <input
              type="text"
              onChange={(e) => setName(e.target.value)}
              style={{ width: "400px", height: "25px" }}
            />
            {error && name.length <= 0 ? (
              <p className={main.popupError1}>This field is required</p>
            ) : (
              ""
            )}
          </label>
          <label>
            <p style={{ marginTop: "10px" }}>Email: </p>
            <input
              type="text"
              onChange={(e) => setEmail(e.target.value)}
              style={{ width: "400px", height: "25px" }}
            />
            { errorEmail ? (
              <p className={main.popupError1}>
                This field is required or email not valid
              </p>
            ) : (
              ""
            )}
          </label>
          <label>
            <p style={{ marginTop: "10px" }}>Message: </p>
            <textarea
              type="text"
              onChange={(e) => setMessage(e.target.value)}
              style={{ width: "400px" }}
            />
            {error && message.length <= 0 ? <p className={main.popupError1}>This field is required</p> : ""}
          </label>
          <div>
            <button type="submit" className={main.button}>
              SUBMIT
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default TextForm;
