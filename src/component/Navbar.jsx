import main from "../assets/css/main.module.css";

import React from "react";

function navbar() {
  return (
    <div>
      <nav className={main.nav}>
        <label className={main.logo}>Company</label>
        <ul>
          <li>
            <a href="">ABOUT</a>
          </li>
          <li>
            <a href="">OUR WORK</a>
          </li>
          <li>
            <a href="">OUR TEAM</a>
          </li>
          <li>
            <a href="">CONTACT</a>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default navbar;
