import { AiFillFacebook, AiOutlineTwitter } from "react-icons/ai";
import main from "../assets/css/main.module.css";
import React from "react";

function Footer() {
  return (
    <div>
      <footer className={main.footer}>
        <div style={{
            marginBottom:'10px'
        }}>
          <p
            style={{
              fontSize: "11px",
            }}
          >
            Copyright @ 2016.PT Company
          </p>
        </div>
        <div style={{
            textAlign:'center'
        }}>
          <AiFillFacebook style={{marginInlineEnd:'4px'}} />
          <AiOutlineTwitter />
        </div>
      </footer>
    </div>
  );
}

export default Footer;
