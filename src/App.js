import Navbar from './component/Navbar'
import ImgMove from './component/ImgMove'
import Values from './component/Values';
import TextForm from './component/TextForm';
import Footer from './component/Footer';
import './App.css';

function App() {
  return (
    <div >
      <Navbar/>
      <ImgMove/>
      <Values/>
      <TextForm/>
      <Footer/>
    </div>
  );
}

export default App;
